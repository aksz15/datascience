// Vorher alles löschen 
CALL apoc.periodic.iterate('MATCH (n) RETURN n', 
'DETACH DELETE n', {batchSize:1000});
CALL apoc.schema.assert({},{},true) 
YIELD label, key RETURN *;
// Game of Thrones
CREATE CONSTRAINT FOR (c:Character) REQUIRE c.id IS UNIQUE;
UNWIND range(1,7) AS season
LOAD CSV WITH HEADERS FROM "https://git.thm.de/aksz15/data/-/raw/master/gds-sample-data/got-s" + season + "-nodes.csv" AS row
MERGE (c:Character {id: row.Id})
ON CREATE SET c.name = row.Label;
UNWIND range(1,7) AS season
LOAD CSV WITH HEADERS FROM "https://git.thm.de/aksz15/data/-/raw/master/gds-sample-data/got-s" + season + "-edges.csv" AS row
MATCH (source:Character {id: row.Source})
MATCH (target:Character {id: row.Target})
CALL apoc.merge.relationship(source, "INTERACTS_SEASON" + season, {}, {}, target) YIELD rel
SET rel.weight = toInteger(row.Weight);
// European Roads
CREATE CONSTRAINT FOR (p:Place) REQUIRE p.name IS UNIQUE;
LOAD CSV WITH HEADERS FROM "https://git.thm.de/aksz15/data/-/raw/master/gds-sample-data/roads.csv"
AS row
MERGE (origin:Place {name: row.origin_reference_place})
SET origin.countryCode = row.origin_country_code
MERGE (destination:Place {name: row.destination_reference_place})
SET destination.countryCode = row.destination_country_code
MERGE (origin)-[eroad:EROAD {road_number: row.road_number}]->(destination)
SET eroad.distance = toInteger(row.distance), eroad.watercrossing = row.watercrossing;
MATCH (:Place)-[r:EROAD]->(:Place) SET r.inverse_distance = 1.0 / log10(r.distance + 2);
// Recipies
CREATE CONSTRAINT FOR (r:Recipe) REQUIRE r.name IS UNIQUE;
CREATE CONSTRAINT FOR (i:Ingredient) REQUIRE i.name IS UNIQUE;
LOAD CSV WITH HEADERS FROM "https://git.thm.de/aksz15/data/-/raw/master/gds-sample-data/recipes.csv"
AS row
MERGE (r:Recipe{name:row.recipe})
WITH r,row.ingredients as ingredients
UNWIND split(ingredients,',') as ingredient
MERGE (i:Ingredient{name:ingredient})
MERGE (r)-[:CONTAINS_INGREDIENT]->(i);
// PersonsHelpsGraph
MERGE (nAlice:Person {name:'Alice'})
MERGE (nBridget:Person {name:'Bridget'})
MERGE (nCharles:Person {name:'Charles'})
MERGE (nDoug:Person {name:'Doug'})
MERGE (nMark:Person {name:'Mark'})
MERGE (nMichael:Person {name:'Michael'})
MERGE (nAlice)-[:HELPS{weight:5.0}]->(nBridget)
MERGE (nAlice)-[:HELPS{weight:1.5}]->(nCharles)
MERGE (nMark)-[:HELPS{weight:2.0}]->(nDoug)
MERGE (nMark)-[:HELPS{weight:1.7}]->(nMichael)
MERGE (nBridget)-[:HELPS{weight:0.5}]->(nMichael)
MERGE (nDoug)-[:HELPS{weight:1.1}]->(nMark)
MERGE (nMichael)-[:HELPS{weight:0.5}]->(nAlice)
MERGE (nAlice)-[:HELPS{weight:1.1}]->(nMichael)
MERGE (nBridget)-[:HELPS{weight:2.6}]->(nAlice)
MERGE (nMichael)-[:HELPS{weight:1.8}]->(nBridget);
